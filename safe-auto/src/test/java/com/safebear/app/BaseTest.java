package com.safebear.app;

import com.safebear.app.pages.*;
import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class BaseTest {
    WebDriver driver;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userpage;
    FramesPage framesPage;
    FramesPageMainFrame framesPageMainFrame;
@Before
public void setup() throws MalformedURLException {
   driver = new ChromeDriver();
    //DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
    //driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"),capabilities);
    Utils utility;
    utility = new Utils();
    welcomePage = new WelcomePage(driver);
    loginPage = new LoginPage(driver);
    userpage= new UserPage(driver);
    framesPage = new FramesPage(driver);
    framesPageMainFrame = new FramesPageMainFrame(driver);
    assertTrue(utility.navigateToWebsite(driver));
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    driver.manage().window().maximize();
}

@After
public void teardown(){
    try {
        TimeUnit.SECONDS.sleep(4);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    driver.quit();
}
}
