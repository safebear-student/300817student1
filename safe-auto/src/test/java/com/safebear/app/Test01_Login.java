package com.safebear.app;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class Test01_Login extends BaseTest {
    @Test
    public void testLogin(){
        Assert.assertTrue(true);
        Assert.assertTrue(welcomePage.checkCorrectpage());
        Assert.assertTrue(welcomePage.clickOnLogin(this.loginPage));
        Assert.assertTrue(loginPage.login(this.userpage,"testuser","testing"));
    }
}
