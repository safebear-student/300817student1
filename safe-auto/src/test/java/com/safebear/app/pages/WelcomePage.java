package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class WelcomePage {
    WebDriver driver;
    @FindBy(xpath = "//*[contains(@title,'Welcome')]")
    WebElement longLink2;


    @FindBy(xpath = "//a[contains(text(),'Login')]")
    WebElement longLink3;




    public WelcomePage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectpage(){
        return driver.getTitle().startsWith("Welcome");
    }

    public boolean clickOnLogin(LoginPage login){
        longLink3.click();
        return login.checkCorrectPage();
    }

}
