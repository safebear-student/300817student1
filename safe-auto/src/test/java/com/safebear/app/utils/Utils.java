package com.safebear.app.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.lang.management.BufferPoolMXBean;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class Utils {
    WebDriver driver;
    String url;

    public boolean navigateToWebsite(WebDriver driver){
        this.driver = driver;
        url="http://automate.safebear.co.uk";
        driver.get(url);
//        driver.findElement(By.xpath("//a[contains(text(),'Login')]")).click();
//        driver.findElement(By.xpath("//*[@id='myid']")).sendKeys("testuser");
//        driver.findElement(By.xpath("//*[@id='mypass']")).sendKeys("testing");
//        driver.findElement(By.xpath("//*[@type='submit']")).click();
        return driver.getTitle().startsWith("Welcome");
    }

}
